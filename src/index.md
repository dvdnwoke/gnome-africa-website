---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "GNOME Africa"
  text: "Growing GNOME Communities in Africa"
  tagline: With smile and community spirit
  actions:
    - theme: brand
      text: I want to contribute
      link: /contribute
    - theme: alt
      text: See our activites
      link: /activities

# features:
#   - title: Feature A
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#   - title: Feature B
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#   - title: Feature C
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---