Thank you for your interest to contribute.

## How to contribute

### Setup your enviroment
This enviroment setup is assumed you are using ssh because password and username authentication should be straight forward. In the incase it's not for you open an issue we will guide you.
#### Windows 11
Open your terminal and execute

`ssh-keygen`

You will be prompted with

`Enter File in which to save the key`

If you want the default name for the file leave it empty and press enter or you name it if you need.

Note: Take not of the name

Next prompt will be for password. Enter password or leave it empty it's optional.

Note: Use a password you can remember.

After this your keys are created.

On the prompt look for where your key is saved if you didn't change the default name it should be name `id_rsa` and `id_rsa.pub`

Copy the `id_rsa.pub`

Visit your GNOME Gitlab Settings page.
Click on SSH KEY and paste your copied key.

Execute the code below to test all is well.

`SSH -T git@ssh.gitlab.gnome.org`

If all went well you would see a welcome message.
