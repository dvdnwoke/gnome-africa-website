
export default {
  lang: 'en-US',
  title: 'Gnome Africa',
  description: 'Vite & Vue powered static site generator.',
  srcDir: 'src',
  cleanUrls: true,
  themeConfig: {
    siteTitle: false,
    logo: {
      light: './image/logo/black-logo.png',
      dark: './image/logo/white-logo.png'
    },
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Activites', link: '/activities' },
      { text: 'What is Gnome Africa', link: '/about_us' },
      { text: 'Gallery', link: '/gallery' },
    ]
  },
}