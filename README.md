# Gnome Africa
Welcome to Gnome Africa Webpage source code your contibution is needed.

To run this code you will need nodejs Version 20 upward installed.

## Clone with SSH

Run the code

`git clone git@ssh.gitlab.gnome.org:dvdnwoke/gnome-africa-website.git`

## Clone with HTTP

Run the code

`git clone https://gitlab.gnome.org/dvdnwoke/gnome-africa-website.git`

## Install dependency

Run in the cloned folder

`npm install`

## Run the code

`npm run docs:dev`

## Contributing
To contribute please read up [Our Contribution Page](CONTRIBUTING.md)

